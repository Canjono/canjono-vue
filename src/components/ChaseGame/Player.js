export default class Player {
  id = 'canjono'
  speed = 160
  image = 'chase-game/canjono2.png'
  gameObject

  constructor() {}

  init(gameObject) {
    this.gameObject = gameObject
    this.gameObject.setCollideWorldBounds(true)
  }

  checkInput(cursors) {
    if (cursors.right.isDown) {
      this.gameObject.setVelocityX(160)
    } else if (cursors.left.isDown) {
      this.gameObject.setVelocityX(-160)
    } else {
      this.gameObject.setVelocityX(0)
    }
  }
}
