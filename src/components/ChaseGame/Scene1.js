import Phaser from 'phaser'
import Player from './Player'

export default class Scene1 extends Phaser.Scene {
	player
	canvasHeight
	canvasWidth
	cursors

	constructor(canvasWidth, canvasHeight) {
		super()

		this.canvasWidth = canvasWidth
		this.canvasHeight = canvasHeight
	}

	preload() {
		this.objects = {}
		this.player = new Player()

		this.load.image(this.player.id, this.player.image)
	}

	create() {
		this.setupBackground()
		this.setupPlayer()

		this.cursors = this.input.keyboard.createCursorKeys()
	}

	update() {
		this.player.checkInput(this.cursors)
	}

	setupBackground() {
		this.objects.camera = this.cameras.add(
			0,
			0,
			this.canvasWidth,
			this.canvasHeight
		)
		this.objects.camera.setBackgroundColor('#000')
	}

	setupPlayer() {
		const playerGameObject = this.physics.add
			.sprite(100, 100, this.player.id)
			.setScale(1.5)

		this.player.init(playerGameObject)
	}
}
