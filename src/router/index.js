import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home/Home.vue'
import CssCraziness from '../components/CssCraziness/CssCraziness.vue'
import FakeSites from '../components/FakeSites/FakeSites.vue'
import Games from '../components/Games/Games.vue'
import SnakeGame from '../components/SnakeGame/SnakeGame.vue'
import SpaceInvadersGame from '../components/SpaceInvadersGame/SpaceInvadersGame.vue'
import About from '../components/About/About.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/css-craziness', component: CssCraziness },
  { path: '/fake-sites', component: FakeSites },
  { path: '/games', component: Games },
  { path: '/games/snake', component: SnakeGame },
  { path: '/games/space-invaders', component: SpaceInvadersGame },
  { path: '/about', component: About },
]

export default createRouter({
  history: createWebHistory(),
  routes: routes,
})
