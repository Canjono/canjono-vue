export default class StartButton extends Phaser.GameObjects.Text {
  constructor(config) {
    super(config.scene, config.x, config.y, config.text, config.style)
    this.scene.add.existing(this)
    this.onClick = config.onClick

    this.setupEventListeners()
  }

  setupEventListeners() {
    this.setInteractive()

    this.on('pointerover', () => {
      this.setStyle({ fill: 'lightgreen' })
    })
    this.on('pointerout', () => {
      this.setStyle({ fill: 'green' })
    })
    this.on('pointerdown', () => {
      this.onClick()
    })
  }
}
