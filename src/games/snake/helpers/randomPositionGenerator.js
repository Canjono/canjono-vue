export default function randomPositionGenerator({
  gridSize,
  minX,
  minY,
  maxX,
  maxY,
  invalidPositions,
}) {
  while (true) {
    const randomPosition = generateRandomPosition(
      minX,
      minY,
      maxX,
      maxY,
      gridSize
    )
    const isValid = !invalidPositions.find(
      (position) =>
        position.x === randomPosition.x && position.y === randomPosition.y
    )

    if (isValid) {
      return randomPosition
    }
  }
}

function generateRandomPosition(minX, minY, maxX, maxY, gridSize) {
  let randomWidth = Math.random() * (maxX - minX) + minX
  let rest = randomWidth % gridSize
  randomWidth = Math.max(0, randomWidth - rest) + gridSize / 2

  let randomHeight = Math.random() * (maxY - minY) + minY
  rest = randomHeight % gridSize
  randomHeight = Math.max(0, randomHeight - rest) + gridSize / 2

  return { x: randomWidth, y: randomHeight }
}
