export class Direction {
  static up = 'up'
  static down = 'down'
  static left = 'left'
  static right = 'right'
}
