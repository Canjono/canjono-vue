import Phaser from 'phaser'
import GameScene from './scenes/gameScene'
import GameUIScene from './scenes/gameUIScene'
import MainMenuScene from './scenes/mainMenuScene'

export default class Game {
  start() {
    const mainMenuScene = new MainMenuScene()
    const gameScene = new GameScene()
    const gameUIScene = new GameUIScene()

    const config = {
      type: Phaser.AUTO,
      width: 320,
      height: 400,
      physics: {
        default: 'arcade',
        arcade: {
          gravity: { y: 0 },
        },
        debug: false,
      },
      scene: [mainMenuScene, gameScene, gameUIScene],
      parent: 'game',
      scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
      },
    }

    new Phaser.Game(config)
  }
}
