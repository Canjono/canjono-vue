import { UI } from '../constants/ui'
import scoreEvent from '../events/scoreEvent'

export default class GameUIScene extends Phaser.Scene {
  static id = 'GameUIScene'
  scoreText

  constructor() {
    super(GameUIScene.id)
  }

  create() {
    this.setupScoreText()
    this.setupGameOverText()

    scoreEvent.on('update-score', this.updateScore, this)
    scoreEvent.on('game-over', this.gameOver, this)
  }

  setupScoreText() {
    this.scoreText = this.add
      .text(16, UI.height / 2, 'Score: 0', {
        fill: '#fff',
      })
      .setOrigin(0, 0.5)
  }

  setupGameOverText() {
    this.gameOverText = this.add
      .text(this.game.config.width - 128, UI.height / 2, 'GAME OVER', {
        fill: 'red',
      })
      .setOrigin(0, 0.5)
      .setVisible(false)
  }

  updateScore(score) {
    this.scoreText.text = `Score: ${score}`
  }

  gameOver() {
    this.gameOverText.setVisible(true)
  }
}
