import BodyPart from '../entities/bodyPart'
import scoreEvent from '../events/scoreEvent'
import Food from '../entities/food'
import Player from '../entities/player'
import randomPositionGenerator from '../helpers/randomPositionGenerator'
import GameUIScene from './gameUIScene'
import { UI } from '../constants/ui'

export default class GameScene extends Phaser.Scene {
  static id = 'GameScene'
  foodGroup
  score = 0

  constructor() {
    super(GameScene.id)
  }

  preload() {
    this.load.image(Player.id, Player.image)
    this.load.image(BodyPart.id, BodyPart.image)
    this.load.image(Food.id, Food.image)
  }

  create() {
    this.cursors = this.input.keyboard.createCursorKeys()
    this.setupBackground()
    this.setupPlayer()
    this.setupFood()
    this.listenForInput()
    this.setupUI()
  }

  update() {
    this.player.update()
  }

  setupBackground() {
    this.cameras.main.setBackgroundColor('#000')
  }

  setupPlayer() {
    this.player = new Player({ scene: this, x: 168, x: 168 }).setScale(0.5)
  }

  setupFood() {
    const { x, y } = this.getFoodPosition()
    this.foodGroup = this.physics.add.group({
      key: Food.id,
      active: false,
      visible: true,
      maxSize: 1,
      setXY: { x, y },
      setScale: { x: 0.5, y: 0.5 },
    })

    this.physics.add.collider(
      this.player,
      this.foodGroup,
      () => this.player.onEat(() => this.onEat()),
      null,
      this
    )
  }

  setupUI() {
    const graphics = this.add.graphics()
    graphics.lineStyle(1, 0xffffff)
    graphics.lineBetween(0, UI.height, this.game.config.width, UI.height)

    this.scene.run(GameUIScene.id, { score: this.score })
  }

  onEat() {
    scoreEvent.emit('update-score', ++this.score)
    const { x, y } = this.getFoodPosition()
    this.foodGroup.get(x, y)
  }

  getFoodPosition() {
    return randomPositionGenerator({
      gridSize: this.player.displayWidth,
      minX: 0,
      minY: UI.height,
      maxX: this.game.config.width,
      maxY: this.game.config.height,
      invalidPositions: this.player.getAllPositions(),
    })
  }

  listenForInput() {
    this.input.keyboard.on('keydown', (event) => {
      switch (event.key) {
        case 'p':
          this.player.toggleStop()
          break
      }
    })
  }
}
