import StartButton from '../ui/startButton'
import GameScene from './gameScene'

export default class MainMenuScene extends Phaser.Scene {
  static id = 'MainMenuScene'

  constructor() {
    super(MainMenuScene.id)
  }

  create() {
    this.setupBackground()
    this.setupTitle()
    this.setupStartButton()
    this.setupControlDescription()
  }

  setupBackground() {
    this.cameras.main.setBackgroundColor('#000')
  }

  setupTitle() {
    const x = this.game.config.width / 2
    const y = this.game.config.height * 0.3
    this.add
      .text(x, y, 'SNAKE', { fill: '#fff' })
      .setOrigin(0.5, 0.5)
      .setScale(1.7)
  }

  setupStartButton() {
    const x = this.game.config.width / 2
    const y = this.game.config.height * 0.5
    new StartButton({
      scene: this,
      x,
      y,
      text: 'START',
      style: { fill: 'green' },
      onClick: () => this.startGame(),
    })
      .setOrigin(0.5, 0.5)
      .setScale(1.2)

    this.cameras.main.once(
      Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE,
      (cam, effect) => {
        this.scene.start(GameScene.id)
      }
    )
  }

  setupControlDescription() {
    const x = this.game.config.width / 2
    const y = this.game.config.height * 0.8
    this.add
      .text(
        x,
        y,
        'Swipe up, down, left and right to control the snake. Or use the key arrows.',
        {
          fill: '#fff',
          align: 'center',
        }
      )
      .setOrigin(0.5, 0.5)
      .setScale(0.9)
      .setWordWrapWidth(250)
  }

  startGame() {
    this.cameras.main.fadeOut(500, 0, 0, 0)
  }
}
