import BodyPart from './bodyPart'
import { Direction } from '../constants/direction'
import scoreEvent from '../events/scoreEvent'
import { UI } from '../constants/ui'

export default class Player extends Phaser.GameObjects.Sprite {
  static id = 'player'
  static image = '/snake-game/snake-head.png'
  direction = Direction.down
  moveUpdateTime = 200
  moveUpdateDecrease = 0.98
  lastMovement = Date.now()
  bodyParts = []
  inputWasDown = false

  constructor(config) {
    super(config.scene, config.x, config.y, Player.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
  }

  update() {
    if (!this.active) {
      return
    }
    this.checkInput()
    this.move()
  }

  checkInput() {
    this.checkCursors()
    this.checkTouch()
  }

  checkCursors() {
    if (
      this.scene.cursors.right.isDown &&
      this.isAllowedToMove(Direction.right)
    ) {
      this.direction = Direction.right
    } else if (
      this.scene.cursors.left.isDown &&
      this.isAllowedToMove(Direction.left)
    ) {
      this.direction = Direction.left
    } else if (
      this.scene.cursors.up.isDown &&
      this.isAllowedToMove(Direction.up)
    ) {
      this.direction = Direction.up
    } else if (
      this.scene.cursors.down.isDown &&
      this.isAllowedToMove(Direction.down)
    ) {
      this.direction = Direction.down
    }
  }

  checkTouch() {
    if (this.scene.input.activePointer.isDown) {
      this.inputWasDown = true
      return
    }
    if (!this.inputWasDown) {
      return
    }
    this.inputWasDown = false
    if (this.scene.input.activePointer.getDistanceY() > 40) {
      if (
        this.scene.input.activePointer.upY <
          this.scene.input.activePointer.downY &&
        this.isAllowedToMove(Direction.up)
      ) {
        this.direction = Direction.up
      } else if (this.isAllowedToMove(Direction.down)) {
        this.direction = Direction.down
      }
    } else if (this.scene.input.activePointer.getDistanceX() > 40) {
      if (
        this.scene.input.activePointer.upX <
          this.scene.input.activePointer.downX &&
        this.isAllowedToMove(Direction.left)
      ) {
        this.direction = Direction.left
      } else if (this.isAllowedToMove(Direction.right)) {
        this.direction = Direction.right
      }
    }
  }

  move() {
    const now = Date.now()
    const delta = now - this.lastMovement

    if (delta < this.moveUpdateTime) {
      return
    }

    this.updateBodyParts()
    this.lastMovement = this.lastMovement + delta

    switch (this.direction) {
      case Direction.up:
        this.moveUp()
        break
      case Direction.down:
        this.moveDown()
        break
      case Direction.left:
        this.moveLeft()
        break
      case Direction.right:
        this.moveRight()
        break
    }

    const { x, y } = this.getCenter()

    if (this.isBodyPosition(x, y)) {
      this.toggleStop()
      console.log('died')
    }
  }

  moveUp() {
    const { x, y } = this.getNextPosition(Direction.up)
    this.setPosition(x, y)
    this.setFlipY(false)
    this.setAngle(0)
  }

  moveDown() {
    const { x, y } = this.getNextPosition(Direction.down)
    this.setPosition(x, y)
    this.setFlipY(true)
    this.setAngle(0)
  }

  moveRight() {
    const { x, y } = this.getNextPosition(Direction.right)
    this.setPosition(x, y)
    this.setFlipY(false)
    this.setAngle(90)
  }

  moveLeft() {
    const { x, y } = this.getNextPosition(Direction.left)
    this.setPosition(x, y)
    this.setFlipY(false)
    this.setAngle(-90)
  }

  isBodyPosition(x, y) {
    return this.bodyParts.find((bodyPart) => {
      const position = bodyPart.getCenter()
      return position.x === x && position.y === y
    })
  }

  getNextPosition(direction) {
    let { x, y } = this.getCenter()

    switch (direction) {
      case Direction.up:
        y = y - this.displayWidth
        y = y < UI.height ? this.scene.game.config.height - UI.height + y : y
        break
      case Direction.down:
        y = y + this.displayWidth
        y =
          y > this.scene.game.config.height
            ? y - this.scene.game.config.height + UI.height
            : y
        break
      case Direction.left:
        x = x - this.displayWidth
        x = x < 0 ? this.scene.game.config.width + x : x
        break
      case Direction.right:
        x = x + this.displayWidth
        x =
          x > this.scene.game.config.width
            ? x - this.scene.game.config.width
            : x
        break
    }

    return { x, y }
  }

  toggleStop() {
    scoreEvent.emit('game-over')
    this.setActive(!this.active)
  }

  onEat(callback) {
    this.addBodyPart()
    this.moveUpdateTime = this.moveUpdateTime * this.moveUpdateDecrease
    callback()
  }

  addBodyPart() {
    const { x, y } =
      this.bodyParts.length === 0
        ? this.getCenter()
        : this.bodyParts[this.bodyParts.length - 1].getCenter()
    const bodyPart = new BodyPart({ scene: this.scene, x, y }).setScale(0.5)
    this.bodyParts.push(bodyPart)
  }

  updateBodyParts() {
    for (let i = this.bodyParts.length - 1; i >= 0; i--) {
      const bodyPart = this.bodyParts[i]
      const { x, y } =
        i === 0 ? this.getCenter() : this.bodyParts[i - 1].getCenter()

      bodyPart.setPosition(x, y)
    }
  }

  getAllPositions() {
    return [
      this.getCenter(),
      ...this.bodyParts.map((bodyPart) => bodyPart.getCenter()),
    ]
  }

  isAllowedToMove(direction) {
    if (this.bodyParts.length === 0) {
      return true
    }
    const { x, y } = this.getNextPosition(direction)
    const forbiddenPos = this.bodyParts[0].getCenter()

    return forbiddenPos.x !== x || forbiddenPos.y !== y
  }
}
