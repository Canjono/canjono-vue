export default class BodyPart extends Phaser.GameObjects.Sprite {
  static id = 'bodyPart'
  static image = '/snake-game/snake-body.png'

  constructor(config) {
    super(config.scene, config.x, config.y, BodyPart.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
  }
}
