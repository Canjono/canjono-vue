export class ControlButton extends Phaser.GameObjects.Sprite {
  static id = 'controlButton'
  static spritesheet = '/space-invaders-game/input.png'

  constructor(scene, x, y) {
    super(scene, x, y, ControlButton.id)
    this.scene.add.existing(this)
  }
}

export class LeftButton extends ControlButton {
  static id = 'leftButton'

  constructor(scene, x, y) {
    super(scene, x, y, ControlButton.id)
  }
}

export class RightButton extends ControlButton {
  static id = 'rightButton'

  constructor(scene, x, y) {
    super(scene, x, y, ControlButton.id)
  }
}

export class ShootButton extends ControlButton {
  static id = 'shootButton'

  constructor(scene, x, y) {
    super(scene, x, y, ControlButton.id)
  }
}
