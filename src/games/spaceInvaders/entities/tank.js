import customEvents, { CustomEventNames } from '../events/customEvents'
import { TankBullets } from './tankBullet'

export default class Tank extends Phaser.Physics.Arcade.Sprite {
  static id = 'tank'
  static image = '/space-invaders-game/tank.png'

  speed = 100
  bullets
  isMovingRight = false
  isMovingLeft = false

  constructor(config) {
    super(config.scene, config.x, config.y, Tank.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
    this.cursors = this.scene.input.keyboard.createCursorKeys()

    this.bullets = new TankBullets(this.scene)

    this.scene.input.keyboard.on('keydown-SPACE', () => {
      this.bullets.fire(this.x, this.y)
    })

    this.setCollideWorldBounds(true)

    this.setSize(32, 24)
    this.setOffset(0, 8)

    this.listenForTouchInput()
  }

  update() {
    this.move()
  }

  move() {
    let velocityX = 0
    if (this.isMovingRight || this.cursors.right.isDown) {
      velocityX += this.speed
    }
    if (this.isMovingLeft || this.cursors.left.isDown) {
      velocityX -= this.speed
    }
    this.setVelocityX(velocityX)
  }

  listenForTouchInput() {
    customEvents.on(CustomEventNames.MoveLeft, this.moveLeft, this)
    customEvents.on(CustomEventNames.MoveRight, this.moveRight, this)
    customEvents.on(CustomEventNames.Shoot, this.shoot, this)
  }

  moveLeft(isMoving) {
    this.isMovingLeft = isMoving
  }

  moveRight(isMoving) {
    this.isMovingRight = isMoving
  }

  shoot() {
    this.bullets.fire(this.x, this.y)
  }
}
