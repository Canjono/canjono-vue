export class TankBullet extends Phaser.Physics.Arcade.Sprite {
  static id = 'tankBullet'
  static image = '/space-invaders-game/tankBullet.png'

  speed = -300

  constructor(scene, x, y) {
    super(scene, x, y, TankBullet.id)
  }

  preUpdate() {
    if (this.y < 0) {
      this.setActive(false)
      this.setVisible(false)
    }
  }

  fire(x, y) {
    this.body.reset(x, y)
    this.setActive(true)
    this.setVisible(true)
    this.setVelocity(0, this.speed)
  }

  onHit() {
    this.setActive(false)
    this.setVisible(false)
    this.setVelocity(0, 0)
  }
}

export class TankBullets extends Phaser.Physics.Arcade.Group {
  constructor(scene) {
    super(scene.physics.world, scene)

    this.createMultiple({
      frameQuantity: 1,
      key: TankBullet.id,
      active: false,
      visible: false,
      setScale: { x: 0.2, y: 0.2 },
      classType: TankBullet,
    })
  }

  fire(x, y) {
    const bullet = this.getFirstDead()

    if (bullet) {
      bullet.fire(x, y)
    }
  }
}
