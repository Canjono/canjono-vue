import Alien from './alien'

export default class Alien3 extends Alien {
  static id = 'alien3'

  points = 30

  constructor(scene, x, y) {
    super(scene, x, y, Alien3.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
    this.setSize(32, 16)
    this.setOffset(0, 0)

    this.setupAnim()
  }

  setupAnim() {
    this.anims.create({
      key: Alien3.id,
      frameRate: 0,
      frames: this.anims.generateFrameNumbers(Alien.spritesheetId, {
        start: 4,
        end: 5,
      }),
      repeat: -1,
    })

    this.play(Alien3.id)
  }
}
