import Alien from './alien'

export default class Alien1 extends Alien {
  static id = 'alien1'

  points = 10

  constructor(scene, x, y) {
    super(scene, x, y, Alien1.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
    this.setSize(32, 16)
    this.setOffset(0, 0)

    this.setupAnim()
  }

  setupAnim() {
    this.anims.create({
      key: Alien1.id,
      frameRate: 0,
      frames: this.anims.generateFrameNumbers(Alien.spritesheetId, {
        start: 0,
        end: 1,
      }),
      repeat: -1,
    })

    this.play(Alien1.id)
  }
}
