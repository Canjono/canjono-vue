export default class Alien extends Phaser.Physics.Arcade.Sprite {
  static spritesheetId = 'aliens'
  static spritesheet = '/space-invaders-game/aliens.png'
  static frameWidth = 32
  static frameHeight = 16

  stepX = 2
  stepY = 16

  points = 0

  delayedCall

  move(delay) {
    this.delayedCall = this.scene.time.delayedCall(
      delay,
      this._move,
      null,
      this
    )
  }

  _move() {
    if (!this.active) {
      return
    }
    const center = this.getCenter()
    this.setX(center.x + this.stepX)
    this.anims.nextFrame()
  }

  onHit() {
    this.destroy()
  }
}

export class AlienGroup extends Phaser.Physics.Arcade.Group {
  moveDelayStep = 18
  deltaSinceMovement = 0
  movementTime = 1000

  constructor(scene, y, key, classType) {
    super(scene.physics.world, scene)

    this.createMultiple({
      frameQuantity: 11,
      key,
      active: true,
      visible: true,
      setScale: { x: 1, y: 1 },
      setXY: { x: 24, y, stepX: 40 },
      classType,
    })
  }

  update(delta, groupDelay) {
    this.deltaSinceMovement += delta

    if (this.deltaSinceMovement < this.movementTime) {
      return
    }

    this.scene.time.delayedCall(
      groupDelay.value,
      () => this.moveAliens(groupDelay.value),
      null,
      this
    )

    this.deltaSinceMovement = 0
    groupDelay.value += this.getChildren().length * this.moveDelayStep
  }

  moveAliens(groupDelay) {
    this.getChildren().forEach((alien, i) => {
      const delay = this.moveDelayStep * i
      alien.move(delay + groupDelay)
    })
  }
}
