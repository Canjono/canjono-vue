import Alien from './alien'

export default class Alien2 extends Alien {
  static id = 'alien2'

  points = 20

  constructor(scene, x, y) {
    super(scene, x, y, Alien2.id)
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)

    this.setSize(32, 16)
    this.setOffset(0, 0)

    this.setupAnim()
  }

  setupAnim() {
    this.anims.create({
      key: Alien2.id,
      frameRate: 0,
      frames: this.anims.generateFrameNumbers(Alien.spritesheetId, {
        start: 2,
        end: 3,
      }),
      repeat: -1,
    })

    this.play(Alien2.id)
  }
}
