import GameScene from './scenes/gameScene'

export default class Game {
  start() {
    const gameScene = new GameScene()

    const config = {
      type: Phaser.AUTO,
      width: 500,
      height: 400,
      physics: {
        default: 'arcade',
        arcade: {
          gravity: { y: 0 },
          debug: false,
        },
      },
      scene: [gameScene],
      parent: 'game',
      scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
      },
    }

    new Phaser.Game(config)
  }
}
