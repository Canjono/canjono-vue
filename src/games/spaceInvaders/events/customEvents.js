export class CustomEventNames {
  static MoveLeft = 'move-left'
  static MoveRight = 'move-right'
  static Shoot = 'shoot'
}

const customEvents = new Phaser.Events.EventEmitter()

export default customEvents
