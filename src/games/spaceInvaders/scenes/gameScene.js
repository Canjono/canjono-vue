import { TankBullet } from '../entities/tankBullet'
import Tank from '../entities/tank'
import Alien1 from '../entities/alien1'
import Alien2 from '../entities/alien2'
import Alien3 from '../entities/alien3'
import Alien, { AlienGroup } from '../entities/alien'
import {
  ControlButton,
  LeftButton,
  RightButton,
  ShootButton,
} from '../input/controls'
import customEvents, { CustomEventNames } from '../events/customEvents'

export default class GameScene extends Phaser.Scene {
  static id = 'GameScene'
  tank
  alien1
  alien2
  alien3

  constructor() {
    super(GameScene.id)
  }

  preload() {
    this.load.image(Tank.id, Tank.image)
    this.load.image(TankBullet.id, TankBullet.image)
    this.load.spritesheet(Alien.spritesheetId, Alien.spritesheet, {
      frameWidth: Alien.frameWidth,
      frameHeight: Alien.frameHeight,
    })
    this.load.spritesheet(ControlButton.id, ControlButton.spritesheet, {
      frameWidth: 32,
      frameHeight: 32,
    })
  }

  create() {
    this.setupPlayer()
    this.setupAliens()
    this.setupControls()
  }

  update(time, delta) {
    this.tank.update(delta)

    const groupDelay = { value: 0 }
    this.alienGroup5.update(delta, groupDelay)
    this.alienGroup4.update(delta, groupDelay)
    this.alienGroup3.update(delta, groupDelay)
    this.alienGroup2.update(delta, groupDelay)
    this.alienGroup1.update(delta, groupDelay)
  }

  setupPlayer() {
    this.tank = new Tank({
      scene: this,
      x: this.game.config.width / 2,
      y: this.game.config.height - 64,
    }).setScale(0.8)
  }

  setupAliens() {
    const bottomY = this.game.config.height / 2

    this.alienGroup1 = new AlienGroup(
      this,
      bottomY - Alien.frameHeight * 8,
      Alien3.id,
      Alien3
    )

    this.alienGroup2 = new AlienGroup(
      this,
      bottomY - Alien.frameHeight * 6,
      Alien2.id,
      Alien2
    )
    this.alienGroup3 = new AlienGroup(
      this,
      bottomY - Alien.frameHeight * 4,
      Alien2.id,
      Alien2
    )
    this.alienGroup4 = new AlienGroup(
      this,
      bottomY - Alien.frameHeight * 2,
      Alien1.id,
      Alien1
    )
    this.alienGroup5 = new AlienGroup(this, bottomY, Alien1.id, Alien1)

    this.setupBulletAlienColliders()
  }

  setupBulletAlienColliders() {
    this.physics.add.collider(
      this.tank.bullets,
      this.alienGroup1,
      this.onAlienHit,
      null,
      this
    )
    this.physics.add.collider(
      this.tank.bullets,
      this.alienGroup2,
      this.onAlienHit,
      null,
      this
    )
    this.physics.add.collider(
      this.tank.bullets,
      this.alienGroup3,
      this.onAlienHit,
      null,
      this
    )
    this.physics.add.collider(
      this.tank.bullets,
      this.alienGroup4,
      this.onAlienHit,
      null,
      this
    )
    this.physics.add.collider(
      this.tank.bullets,
      this.alienGroup5,
      this.onAlienHit,
      null,
      this
    )
  }

  onAlienHit(bullet, alien) {
    bullet.onHit()
    alien.onHit()
  }

  setupControls() {
    this.input.addPointer(3)
    this.leftButton = new LeftButton(this, 46, this.game.config.height - 32)
      .setScale(1.4)
      .setFrame(0)
      .setInteractive()

    this.leftButton.on('pointerdown', (pointer) => {
      customEvents.emit(CustomEventNames.MoveLeft, true)
      // this.tank.moveLeft()
    })

    this.leftButton.on('pointerup', (pointer) => {
      customEvents.emit(CustomEventNames.MoveLeft, false)
      // this.tank.stopMoveLeft()
    })

    this.rightButton = new RightButton(this, 124, this.game.config.height - 32)
      .setScale(1.4)
      .setFrame(1)
      .setInteractive()

    this.rightButton.on('pointerdown', (pointer) => {
      customEvents.emit(CustomEventNames.MoveRight, true)
      // this.tank.moveRight()
    })

    this.rightButton.on('pointerup', (pointer) => {
      customEvents.emit(CustomEventNames.MoveRight, false)
      // this.tank.stopMoveRight()
    })

    this.shootButton = new ShootButton(
      this,
      this.game.config.width - 46,
      this.game.config.height - 32
    )
      .setScale(1.4)
      .setFrame(2)
      .setInteractive()

    this.shootButton.on('pointerdown', (pointer) => {
      customEvents.emit(CustomEventNames.Shoot)
      // this.tank.shoot()
    })
  }
}
